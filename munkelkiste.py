from flask import Flask, Response, render_template, request
import random
import json
import logging
import os
from datetime import datetime

app = Flask(__name__)

overview_path = '/overview'

DATA_DIR = '/tmp/munkelkiste'

if not os.path.exists(DATA_DIR):
    os.mkdir(DATA_DIR)

if not os.path.isdir(DATA_DIR):
    print('Path of data directory already exists, but is no directory!')
    exit()

def save_rumor(rumor):
    time, text = rumor
    timestamp = str(time.timestamp())
    save_path = os.path.join(DATA_DIR, timestamp)
    with open(save_path, 'w') as file:
        file.write(text)

def load_rumors():
    rumors = []
    for filename in os.listdir(DATA_DIR):
        filepath = os.path.join(DATA_DIR, filename)
        if not os.path.isfile(filepath):
            continue
        try:
            with open(filepath, 'r') as file:
                timestamp = float(filename)
                time = datetime.fromtimestamp(timestamp)
                text = file.read()
                rumors.append((time, text))
        except:
            print(f'Error loading {filepath}')
    return rumors


# rumors [(<submit datetime>, 'text')]
rumors = []

def group_by_lambda(rumor_list: [(datetime, str)], lambda_function) -> [(object, [(datetime, str)])]:
    """Returns the rumors grouped by an arbitrary property using a lambda function.
    """
    reverse = False
    values = set(map(lambda_function, rumor_list))
    grouped = [(value, sorted([record for record in rumor_list if lambda_function(record) == value], reverse=reverse)) for value in values]
    try:
        grouped.sort(reverse=reverse)
    except:
        logging.debug('Grouped rumor list could not be sorted!')
    return grouped



def group_by_day(rumor_list: [(datetime, str)]):
    return group_by_lambda(rumor_list, lambda tup:tup[0].day)

def group_by_hour(rumor_list: [(datetime, str)]):
    return group_by_lambda(rumor_list, lambda tup:tup[0].hour)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/submit', methods = ['POST'])
def submit():
    text = request.form.get('text')
    save_rumor((datetime.now(), text))
    return render_template('submit.html', text=text)

@app.route(overview_path)
def overview():
    print(json.dumps(rumors))
    return render_template('overview.html', rumors=load_rumors(), group_by_day=group_by_day, group_by_hour=group_by_hour)

if __name__ == '__main__':
    app.run()